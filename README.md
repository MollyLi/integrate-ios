## Integrating React Native In IOS APP

介紹在IOS APP的頁面嵌入React Native並且傳送資料給RN

其中透過Storyboard定義view寬高及位置, 可延伸畫面, 不像 `initWithFrame:CGRectMake` 只能指定固定長寬

## 需求

- node.js 4.0 or newer 建議使用 [nvm](https://github.com/creationix/nvm#installation) 安裝
- npm 3.0 or newer 建議更新到最新版
- Xcode 7.0 or higher is required

## Running

`npm install` 安裝ReactNative, 權限不足就要用 `sudo npm install`

`npm start` 啟動開發server

透過XCode開啟`integrate-react-native.xcworkspace`按下Run

## 參考來源
https://medium.com/delivery-com-engineering/react-native-in-an-existing-ios-app-delivered-874ba95a3c52